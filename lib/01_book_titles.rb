class Book
  def title
    @title
  end

  def title=(title)
    @title = titled_book(title)
  end

  def titled_book(title)
    special_words = ["an", "a", "in", "the", "of", "and"]
    book_title = []
    split_string = title.split(" ")
    for i in 0...split_string.length
      word = split_string[i]
      if i == 0
      book_title << word.capitalize
      elsif special_words.include?(word) &&
        (i != 0 || i != split_string.length - 1)
        book_title << word
      else
        book_title << word.capitalize
      end
    end
    return book_title.join(" ")
  end
end
