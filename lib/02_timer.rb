class Timer
  def initialize
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def seconds=(time)
    @seconds = time
  end

  def time_string
    minutes = 0
    hours = 0
    time_s = ""

    while @seconds >= 60
      minutes += 1
      @seconds -= 60
    end

    while minutes >= 60
      hours += 1
      minutes -= 60
    end

    if hours < 10
      time_s << "0#{hours}:"
    else
      time_s << "#{hours}:"
    end

    if minutes < 10
      time_s << "0#{minutes}:"
    else
      time_s << "#{minutes}:"
    end

    if @seconds < 10
      time_s << "0#{@seconds}"
    else
      time_s << "#{@seconds}"
    end
    return time_s
  end
end
